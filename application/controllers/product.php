<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user/user_model','user_model');
        $this->load->library('core_lib');
        $this->load->library('form');
        $this->load->library('default_session');
        $this->load->library('user/user_lib','user_lib');
        $this->load->model('product_model','product_model');

    }


    public function _remap($method)
    {
        $userGroup = $this->core_lib->getUserGroup();
        if($method == "add"){
            if(in_array("admin",$userGroup)){
                $this->add();
            }else{
                $url = base_url()."login/";
                $mode = $this->uri->segment(2);
                if($mode == "edit") {
                    $productId = $this->uri->segment(3);
                    $reUrl = "product/edit/".$productId;
                    $eURL = base64_encode($reUrl);
                    $url .="?redirect=".$eURL;
                }else{
                    $reUrl = "product/add";
                    $eURL = base64_encode($reUrl);
                    $url .="?redirect=".$eURL;
                }
                header('Location:'.$url);
            }
        }elseif ($method == "validate"){
            if(in_array("admin",$userGroup)){
                $this->validate();
            }else{
                die('login required');
            }
        }elseif ($method == "delete"){
            if(in_array("admin",$userGroup)){
                $this->delete();
            }else{
                die('login required');
            }
        }elseif ($method == "uploadProductImage"){
            if(in_array("admin",$userGroup)){
                $this->uploadProductImage();
            }else{
                die('login required');
            }
        }elseif ($method == "getProductImages"){
            if(in_array("admin",$userGroup)){
                $this->getProductImages();
            }else{
                die('login required');
            }
        }elseif ($method == "getProductImageForRender"){
            if(in_array("admin",$userGroup)){
                $this->getProductImageForRender();
            }else{
                die('login required');
            }
        }elseif ($method == "removeImage"){
            if(in_array("admin",$userGroup)){
                $this->removeImage();
            }else{
                die('login required');
            }
        }elseif ($method == "setDefaultImage"){
            if(in_array("admin",$userGroup)){
                $this->setDefaultImage();
            }else{
                die('login required');
            }
        }elseif ($method == "autoSuggest"){
           $this->autoSuggest();
        }elseif ($method == "search"){
           $this->search();
        }elseif ($method == "searchRender"){
           $this->searchRender();
        }elseif ($method == "itemView"){
           $this->itemView();
        }
    }
    public function add()
    {
        $productDetails = [];

        $mode = $this->uri->segment(2);
        if($mode == "edit"){
           $productId = $this->uri->segment(3);
            if($productId){
                $productDetails = $this->product_model->getData($productId);
                if(isset($productDetails['id']) && !empty($productDetails['id'])){
                    $this->load->model('category_model');
                    $categoryDetails = $this->category_model->getData($productDetails['category_id']);
                    $productDetails['category'] = isset($categoryDetails['name'])?$categoryDetails['name'] : 'No category selected';
                    $this->load->model('product_image_model','product_image_model');
                    $productImages = $this->product_image_model->getProductImages($productDetails['id']);
                    if(count($productImages)){
                        foreach ($productImages as $key => $image){
                            $path = base_url();
                            $name = substr($image['file_name'],0,strrpos($image['file_name'],'.'));
                            $ext = substr($image['file_name'],strrpos($image['file_name'],'.'));
                            $thumbPath = $path.'uploads/thumbnail/'.$name.'_thumb'.$ext;
                            $productImages[$key]['thumbnail'] = $thumbPath;
                        }

                    }
                    $productDetails['images'] = $productImages;

                    $this->load->model('product_specification_model','product_specification_model');
                    $fieldsArr = [
                        'product_id'=>$productDetails['id']
                    ];
                    $productSpecification = $this->product_specification_model->listByFields($fieldsArr);
                    $productDetails['specifications'] = $productSpecification;
                    $variantArr = [];
                    $this->load->model('product_variant_model','product_variant_model');
                    $fieldsVariantArr = [
                        'product_id'=>$productDetails['id']
                    ];
                    $productVariantArr = $this->product_variant_model->listByFields($fieldsVariantArr);
                    if(count($productVariantArr)){
                       foreach($productVariantArr as $value){
                           $variantArr[$value['v_type']][] = $value['v_value'];
                       }
                    }
                    $productDetails['variant'] = $variantArr;
                }

            }
        }

        $this->default_session->delete('upload_id');
        $this->default_session->delete('default_image_id');
        $this->load->template('product/edit',$productDetails);
    }

    public function itemView()
    {
        $productId = $this->uri->segment(4);
        $productDetails = $this->product_model->getData($productId);
        if(isset($productDetails['id']) && !empty($productDetails['id'])){
            $this->load->model('product_image_model','product_image_model');
            $productImages = $this->product_image_model->getProductImages($productDetails['id']);
            $productDetails['images'] = $productImages;

            $this->load->model('product_specification_model','product_specification_model');
            $fieldsArr = [
                'product_id'=>$productDetails['id']
            ];
            $productSpecification = $this->product_specification_model->listByFields($fieldsArr);
            $productDetails['specifications'] = $productSpecification;
            $variantArr = [];
            $this->load->model('product_variant_model','product_variant_model');
            $fieldsVariantArr = [
                'product_id'=>$productDetails['id']
            ];
            $productVariantArr = $this->product_variant_model->listByFields($fieldsVariantArr);
            if(count($productVariantArr)){
                foreach($productVariantArr as $value){
                    $variantArr[$value['v_type']][] = $value['v_value'];
                }
            }
            $productDetails['variant'] = $variantArr;
        }
        $latestProducts = $this->product_model->getLatestProducts(5);
        foreach ($latestProducts as $key => $value){
            $productId = $value['id'];
            $thumbPath = '';
            $image = $this->product_image_model->getProductImages($productId,true);
            if(empty($image)){
                $image = $this->product_image_model->getProductImages($productId);
            }
            if(isset($image[0]) && !empty($image[0])){
                $path = base_url();
                $name = substr($image[0]['file_name'],0,strrpos($image[0]['file_name'],'.'));
                $ext = substr($image[0]['file_name'],strrpos($image[0]['file_name'],'.'));
                $thumbPath = $path.'uploads/thumbnail/'.$name.'_thumb'.$ext;
            }
            $latestProducts[$key]['thumbnail'] = $thumbPath;

        }
        $productDetails['latest_products'] = $latestProducts;
        $this->load->template('product/view',$productDetails);
    }
    public function validate()
    {

        $data = json_decode(file_get_contents('php://input'), true);
        $this->form->setRules($data['name'],'product_name_error','Please Enter Product Name ');
        $validationArr = $this->form->run();
        if(is_array($validationArr) && count($validationArr) && $validationArr != false){
            header('Content-Type: application/json');
            echo json_encode( $validationArr );
        }else{
            $variantArr = $data['variant'];
            $formatArray = [];
            if(is_array($variantArr) && count($variantArr)){
                foreach($variantArr as $value){
                    if(is_array($value['value']) && count($value['value'])){
                        foreach($value['value'] as $v){
                            $formatArray[$value['type']][] = $v['value'];
                        }
                    }


                }

            }
            unset($data['variant']);
            $specifications = $data['specification'];
            unset($data['specification']);
            $this->load->model('product_model','product_model');
            if(isset($data['id']) && !empty($data['id'])){
                $temp = [
                    'id' =>$data['id']
                ];
                $this->product_model->update($data,$temp);
            }else{
                $productId = $this->product_model->add($data);

            }
            if(!isset($productId) && empty($productId))$productId = $data['id'];
            $this->load->model('product_specification_model','product_specification_model');
            $this->load->model('product_variant_model','product_variant_model');
            $temp = [
                'product_id' =>$productId
            ];
            $this->product_specification_model->delete($temp,true);
            $this->product_variant_model->delete($temp,true);
            if(count($specifications) && $productId ){
                foreach ($specifications as $value){
                    $temp = [];
                    $temp['product_id'] = $productId;
                    $temp['s_type'] =$value['type'];
                    $temp['s_value'] =$value['value'];
                    $this->product_specification_model->add($temp);
                }
            }

            if(count($formatArray) && $productId ){
                foreach ($formatArray as $key => $value){
                    if(is_array($value) && count($value)){
                       foreach($value as $v){
                           $temp = [];
                           $temp['product_id'] = $productId;
                           $temp['v_type'] =$key;
                           $temp['v_value'] =$v;
                           $this->product_variant_model->add($temp);
                       }
                    }

                }
            }

            $sessionFileIds =  $this->default_session->get('upload_id');
            $this->load->model('product_image_model','product_image_model');
            if($sessionFileIds){
                foreach ($sessionFileIds as $val){
                    $fileDetails['product_id'] = $productId;
                    $fileDetails['file_id'] = $val;
                    $this->product_image_model->add($fileDetails);
                }
            }

            $defaultFileId =  $this->default_session->get('default_image_id');
            if ($defaultFileId) {
                $temp = [
                    'product_id' => $productId
                ];
                $this->product_image_model->update(['set_default' => 0], $temp);

                $temp = [
                    'product_id' => $productId,
                    'file_id' => $defaultFileId
                ];
                $this->product_image_model->update(['set_default' => 1], $temp);
            }
            $result = ['id'=>$productId,'success' =>true];
            header('Content-Type: application/json');
            echo json_encode( $result );
        }



    }

    public function uploadProductImage()
    {
        $this->load->library('upload_lib');
        $resultArr = $this->upload_lib->doUpload();
        if(count($resultArr) && !isset($resultArr['error'])){
            $this->load->model('file_model','file_model');
            $tempData = [
                'file_path' =>$resultArr['upload_data']['full_path'],
                'file_name' =>$resultArr['upload_data']['file_name'],
                'file_extension' =>$resultArr['upload_data']['file_type'],
            ];
            $imgId = $this->file_model->add($tempData);
            if($imgId){
                $oldSession =  $this->default_session->get('upload_id');
                if($oldSession){
                    array_push($oldSession,$imgId);
                }else $oldSession[] = $imgId;

                $this->default_session->set('upload_id', $oldSession);
            }
        }elseif (isset($resultArr['error'])){
            header('Content-Type: application/json');
            echo json_encode( $resultArr );
        }
    }


    public function getProductImages($productId = 0)
    {
        $fileIds = [];
        $productImages = [];
        $sessionFileIds = [];
        if(!empty($productId)){
            $this->load->model('product_image_model','product_image_model');
            $productImages = $this->product_image_model->getProductImages($productId);
        }
        $sessionFileIds =  $this->default_session->get('upload_id');
        if($sessionFileIds){
            foreach ($sessionFileIds as $val){
                $fileIds[] = $val;
            }
        }
        $this->load->model('file_model');
        $fileArr = $this->file_model->getFiles($fileIds);
        $resultFileArr = array_merge($fileArr,$productImages);


        //echo '<pre>';print_r($resultFileArr);die();
        $defaultFileId =  $this->default_session->get('default_image_id');
        if($defaultFileId){
            if($resultFileArr){
                foreach ($resultFileArr as $key => $value){
                    if($value['id'] == $defaultFileId){
                        $resultFileArr[$key]['set_default'] = 1;
                    }else{
                        $resultFileArr[$key]['set_default'] = 0;
                    }
                }
            }
        }


        return $resultFileArr;

    }

    public function getProductImageForRender()
    {
        $productId = $this->input->post('id');
        $imagesList = $this->getProductImages($productId);
        if(count($imagesList)){
            foreach ($imagesList as $key => $image){
                $path = base_url();
                $name = substr($image['file_name'],0,strrpos($image['file_name'],'.'));
                $ext = substr($image['file_name'],strrpos($image['file_name'],'.'));
                $thumbPath = $path.'uploads/thumbnail/'.$name.'_thumb'.$ext;
                $imagesList[$key]['thumbnail'] = $thumbPath;
            }

        }
        $data['images'] = $imagesList;
        $this->load->view('product/image_list',$data);
    }

    public function removeImage()
    {
        $fileId = $this->input->post('file_id');
        if($fileId){
            $this->load->model('file_model','file_model');
           $fileDetails =  $this->file_model->getData($fileId);
            $temp = [
                'id' =>$fileId
            ];
            $this->file_model->delete($temp);
            $path = $fileDetails['file_path'];
            $fileUrl = substr($path,0,strrpos($path,'/'));
            $name = substr($fileDetails['file_name'],0,strrpos($fileDetails['file_name'],'.'));
            $thumbPath = $fileUrl.'/thumbnail/'.$name.'_thumb.png';
            unlink($fileDetails['file_path']);
            unlink($thumbPath);
        }
        $sessionFileIds =  $this->default_session->get('upload_id');
        if(empty($sessionFileIds)){
            $sessionFileIds = [];
        }
        if(in_array($fileId,$sessionFileIds)){
            if(($key = array_search($fileId, $sessionFileIds)) !== false) {
                unset($sessionFileIds[$key]);
            }
            $this->default_session->set('upload_id', $sessionFileIds);
        }elseif($fileId){
            $this->load->model('product_image_model','product_image_model');
            $temp = [
                'file_id' =>$fileId
            ];
            $this->product_image_model->delete($temp);
        }
    }


    public function autoSuggest()
    {
        $suggestionArr = [
            'query'=>'',
            'suggestions'=>''
        ];
        $url = parse_url($_SERVER['REQUEST_URI']);
        parse_str($url['query'], $params);
        if(isset($params['query'])){
            $this->load->model('product_model','product_model');
            $resultArr = $this->product_model->getSuggestion($params['query']);
            if($resultArr){
                $nameArr = array_column($resultArr,'name') ;
                $suggestionArr['query']= $params['query'];
                foreach ($nameArr as $name){
                    $temp = [];
                    $temp['value']=$name;
                    $temp['data']=$name;
                    $suggestionArr['suggestions'][]=$temp;
                }

            }
            header('Content-Type: application/json');
            echo json_encode( $suggestionArr );
        }
    }

    public function listing()
    {
        $result = $this->search();
        $resultArr['data'] = $result;
        $this->load->model('product_model');
        $latestProducts = $this->product_model->getLatestProducts(5);
        foreach ($latestProducts as $key => $value){
            $productId = $value['id'];
            $thumbPath = '';
            $image = $this->product_image_model->getProductImages($productId,true);
            if(empty($image)){
                $image = $this->product_image_model->getProductImages($productId);
            }
            if(isset($image[0]) && !empty($image[0])){
                $path = base_url();
                $name = substr($image[0]['file_name'],0,strrpos($image[0]['file_name'],'.'));
                $ext = substr($image[0]['file_name'],strrpos($image[0]['file_name'],'.'));
                $thumbPath = $path.'uploads/thumbnail/'.$name.'_thumb'.$ext;
            }
            $latestProducts[$key]['thumbnail'] = $thumbPath;

        }
        $resultArr['latest_products'] = $latestProducts;
        $this->load->template('index',$resultArr);
    }

    protected function search()
    {
        $phrase = $this->input->post('phrase');
        $sortBy = $this->input->post('sort_by');
        $categoryId = $this->input->post('category_id');
        $this->load->model('category_model');
        $categoryArr = $this->category_model->categories($categoryId);
        $page = $this->input->post('page');
        if(empty($postData['parent_id']))$postData['parent_id'] = 0;
        $this->load->model('product_model');
        $result =  [];
        if(empty($categoryId) || ($categoryId && !empty($categoryArr))){
            $query = $this->product_model->getSearchQuery($phrase, $sortBy ,0,$categoryArr);
            $result =$this->product_model->getListByQuery($query,$page,'');
        }
        if(isset($result['rows']) && $result['rows']){
            $this->load->model('product_image_model','product_image_model');

          foreach ($result['rows'] as $key => $value){
              $productId = $value['id'];
              $thumbPath = '';
              $orgThumbPath ='';
              $image = $this->product_image_model->getProductImages($productId,true);
              if(empty($image)){
                  $image = $this->product_image_model->getProductImages($productId);
              }
              if(isset($image[0]) && !empty($image[0])){
                  $path = base_url();
                  $name = substr($image[0]['file_name'],0,strrpos($image[0]['file_name'],'.'));
                  $ext = substr($image[0]['file_name'],strrpos($image[0]['file_name'],'.'));
                  $thumbPath = $path.'uploads/thumbnail/'.$name.'_thumb'.$ext;

                  $orgFileName = $image[0]['file_name'];
                  $orgThumbPath = $path.'uploads/'.$orgFileName;
              }
              $result['rows'][$key]['default_image'] = $orgThumbPath;
              $result['rows'][$key]['thumbnail'] = $thumbPath;

          }
        }
        return $result;
    }
    protected function searchRender()
    {
        $result = $this->search();
        $resultArr['data'] = $result;
        $this->load->view('product/search_result',$resultArr);
    }

    protected function setDefaultImage()
    {
        $fileId = $this->input->post('file_id');
        $productId = $this->input->post('product_id');
        $this->load->model('product_image_model','product_image_model');
        $fieldArr = [
            'file_id' =>$fileId
        ];
        $isFileExistInProduct = $this->product_image_model->listByFields($fieldArr);
        if($productId && $isFileExistInProduct){
            $temp = [
                'product_id' =>$productId
            ];
            $this->product_image_model->update(['set_default'=>0],$temp);

            $temp = [
                'product_id' =>$productId,
                'file_id' =>$fileId
            ];
            $this->product_image_model->update(['set_default'=> 1],$temp);
            $this->default_session->delete('default_image_id');
        }else{
            $this->default_session->set('default_image_id', $fileId);
        }


    }

    public function delete()
    {
        $productId = $this->input->post('product_id');
        if ($productId) {
            $this->load->model('product_model', 'product_model');
            $temp = [
                'id' =>$productId
            ];
            $this->product_model->delete($temp);
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */