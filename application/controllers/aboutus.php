<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aboutus extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user/user_model','user_model');
        $this->load->library('core_lib');
        $this->load->library('form');
        $this->load->library('user/user_lib','user_lib');
    }

    public function _remap($method)
    {
        $userGroup = $this->core_lib->getUserGroup();
        if($method == "add"){
            if(in_array("admin",$userGroup)){
                $this->add();
            }else{
                die('login required');
            }
        }elseif ($method == "validate"){
            if(in_array("admin",$userGroup)){
                $this->validate();
            }else{
                die('login required');
            }
        }elseif ($method == "itemView"){
            $this->itemView();

        }
    }
    public function add()
    {
        $aboutusDetails = [];
        $this->load->model('aboutus_model');
        $aboutusDetails['data'] = $this->aboutus_model->getAll();

        $this->load->model('member_model');
        $membersDetails = $this->member_model->getAll();
        if($membersDetails){
          foreach($membersDetails as $key => $val){
              if($val['file_id']){
                  $this->load->model('file_model');
                  $membersFile = $this->file_model->getData($val['file_id']);
                  if($membersFile){
                      $membersDetails[$key]['image'] = $membersFile['file_path'];
                  }
              }

          }
        }
        $aboutusDetails['members'] = $membersDetails;
        $this->load->template('aboutus_edit',$aboutusDetails);

    }

    public function itemView()
    {
        $aboutusDetails = [];
        $this->load->model('aboutus_model');
        $aboutusDetails['data'] = $this->aboutus_model->getAll();

        $this->load->model('member_model');
        $membersDetails = $this->member_model->getAll();
        if($membersDetails){
            foreach($membersDetails as $key => $val){
                if($val['file_id']){
                    $this->load->model('file_model');
                    $membersFile = $this->file_model->getData($val['file_id']);
                    if($membersFile){
                        $membersDetails[$key]['image'] = $membersFile;
                    }
                }

            }
        }
        $aboutusDetails['members'] = $membersDetails;
        $this->load->template('about_us',$aboutusDetails);
    }


    public function validate()
    {
        $postData = [
            'id' =>$this->input->post('id'),
            'introduction' =>$this->input->post('introduction'),
            'mission' =>$this->input->post('mission'),
            'objectives' =>$this->input->post('objectives'),
            'principles' =>$this->input->post('principles'),
        ];


            $this->load->model('aboutus_model');
            if(isset($postData['id']) && !empty($postData['id'])){
                $temp = [
                    'id' =>$postData['id']
                ];
                $this->aboutus_model->update($postData,$temp);
            }else{
                $aboutusId = $this->aboutus_model->add($postData);

            }
            if(!isset($aboutusId) && empty($aboutusId))$aboutusId = $postData['id'];
            $result = ['id'=>$aboutusId,'success' =>true];
            header('Content-Type: application/json');
            echo json_encode( $result );

    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */