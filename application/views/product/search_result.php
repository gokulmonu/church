<?php
$admin = false;
$userGroup= $this->core_lib->getUserGroup();
if(in_array("admin",$userGroup)){
    $admin  = true;
}
if(isset($data)&& isset($data['rows']) && count($data['rows'])){
    foreach ($data['rows'] as $value){
        $date = date('Y-m-d H:i:s',strtotime('yesterday'));
        ?>


        <div class="s-col-lg-3 s-col-md-3 s-col-sm-6 s-col-xs-12"><!--f col-->

            <article class="each-content-block s-wrp"><!--each-content-block-->
                <?php
                $img = !empty($value['default_image'])?$value['default_image'] :$this->config->item('default_image_url');
                $thumbimg = !empty($value['thumbnail'])?$value['thumbnail']:$this->config->item('default_thumb_image_url');
                ?>
                <div class="image-header s-wrp"><!--image-header-->
                    <a href="#"><img src="<?php echo $img?>" alt="image"></a>
                </div><!--/. image-header-->

                <div class="featured-content"><!--featured-content-->

                    <div class="feat-title-wrp s-wrp"><!--feat-title-wrp-->


                        <h2 class="feat-title"><?php echo $value['name'] ?></h2>

                    </div><!--/. feat-title-wrp-->

                    <div class="feat-content-block"><!--feat-content-block-->

                        <p><?php if (strlen($value['description']) > 30) {
                                    echo substr($value['description'], 0, 30) . '..';
                                } else {
                                    echo $value['description'];
                                } ?></p>

                        <a href="<?php echo base_url().'product/view/'.$value['name'].'/'.$value['id']?>" class="link-txt">View</a>
                        <?php
                        if($admin){
                            ?>
                            <a href="<?php echo base_url().'product/edit/'.$value['id']?>" class="link-txt"  title="Edit">EDIT</a>

                            <?php
                        }
                        ?>
                    </div><!--/. feat-content-block-->

                </div><!--/. featured-content-->

            </article><!--/. each-content-block-->

        </div><!--/. f col-->

    <?php
    }
}else{
    ?>
    <span>No Products Found</span>
    <?php
}

if(isset($data) && isset($data['pagination'])){
    $dataArr = [
        'pagination'=>$data['pagination']
    ];
    $this->view('pagination',$dataArr);
}

?>
