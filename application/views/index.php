<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script>

    var currentPage = 1;
    $body = $("body");

   /* $(document).on({
        ajaxStart: function() { $body.addClass("loading");    },
        ajaxStop: function() { $body.removeClass("loading"); }
    });*/
    $(function () {
        getTree();
        $('#auto_suggest').autocomplete({
            serviceUrl: '<?php echo base_url()?>product/auto-suggest/search',
            tabDisabled : true,
            onSelect: function (suggestion) {
                $('#phrase').val(suggestion.value);
                search(1);
            }
        });
        $('#auto_suggest').on('keyup', function (event) {
            event.preventDefault();
            var phrase = $('#auto_suggest').val();
            if(phrase.trim() == ''){
                search(1);
            }
        });
        $('#sort_by').on('change', function (event) {
            event.preventDefault();
                search(1);
        });

        $('#reset_tree').on('click', function (event) {
            event.preventDefault();
            $('#category_id').val('');
                getTree();
                search(1);
        });
    });
    function search (pagenum) {
        $body.addClass("loading");
        var phrase = $('#phrase').val();
        var sortOptions = $('#sort_by').val();
        var categoryId = $('#category_id').val();
        var autoSuggest = $('#auto_suggest').val();
        if(phrase != autoSuggest){
            phrase = autoSuggest;
        }
        $.ajax({
            url: '<?php echo base_url()?>product/search',
            type: 'POST',
            dataType: "html",
            data:{
                'phrase':phrase,
                'page':pagenum,
                'sort_by':sortOptions,
                'category_id' :categoryId

            },
            success: function (data) {
                $body.removeClass("loading");
                $('#product_container').html(data);
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }
    function openPage(pagenum) {
        //currentPage =pagenum;
        search(pagenum);
    }
    function getTree() {

        $.ajax({
            url: '<?php echo base_url()?>category/getTree',
            type: 'POST',
            success: function (data) {
                $('#tree').treeview({
                    data: data,
                    levels:0,
                    onNodeSelected:function (event, data) {
                        $('#category_id').val(data.href);
                        search(1);
                    }

                });
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }
</script>
	<input type="hidden" id="category_id" value="">


    <section class="slider-wrp s-wrp"><!--slider wrp-->

        <div class="flexslider left"><!--flexslider-->

            <ul class="slides">

                <li>
                    <span class="s-full-img"><img src="<?php echo base_url() ?>bootstrap/images/slider1.jpg" alt="slider image"></span>

                    <div class="meta slide-meta"><!--meta-->

                        <h1>Raising the Standard.</h1>
                        <p>As energy prices vary and continue their exponential rise in cost, the need for high quality assistance in planning and implementing</p>

                    </div><!--/. meta-->

                </li>

                <li>
                    <span class="s-full-img"><img src="<?php echo base_url() ?>bootstrap/images/slider2.jpg" alt="slider image"></span>

                    <div class="meta slide-meta"><!--meta-->

                        <h1>Bringing Excellence.</h1>
                        <p>Glatco’s mission is to deliver a competitive and sustainable product </p>

                    </div><!--/. meta-->


                </li>

                <li>
                    <span class="s-full-img"><img src="<?php echo base_url() ?>bootstrap/images/slider3.jpg" alt="slider image"></span>

                    <div class="meta slide-meta"><!--meta-->

                        <h1>Deliver More Value.</h1>
                        <p>GlatCo. would rather customize them to fit the customer's business</p>

                    </div><!--/. meta-->

                </li>



            </ul>

        </div><!--/. flexslider-->

    </section><!--/. slider-wrp-->


    <section class="s-wrp s-hi-pad main-block-wrp"><!--main-block-wrp-->

        <div class="s-container"><!--s-container-->

            <div class="s-col-lg-3 s-col-md-3 s-col-sm-12 s-col-xs-12"><!--f col-->

                <article class="s-wrp side-bar-sec  animated fadeInLeft"><!--side-bar-sec-->

                    <div class="s-wrp each-block"><!--each-block-->


                        <h4 class="side-bar-title s-sec-block">CATEGORIES</h4>
                        <ul class="list-group">
                            <li class="list-group-item node-tree" style="color:#FFFFFF;background-color:#0caad1;cursor: pointer;text-align: center" id="reset_tree">
                                RESET
                            </li></ul>
                        <div id="tree"><!--tree-->

                        </div><!--tree-->



                    </div><!--/. each-block-->

</article>

            </div><!--/. f col-->

            <div class="s-col-lg-9 s-col-md-9 s-col-sm-12 s-col-xs-12"><!--f col-->


                <div class="s-wrp search-box">
                    <input type="text" placeholder="Search" id="auto_suggest"/>
                    <input type="hidden" id="phrase" value=""/>
                    <div id="search_but"  onclick="search(1)"> </div>
                </div>




            <article class="s-wrp content-wrp s-hi-pad"><!--content-wrp-->

                <div class="s-row" id="product_container"><!--s-row-->

                    <?php  $this->view('product/search_result',$data) ?>

                </div><!--/. s-row-->

            </article><!--/. content-wrp-->


       </div><!--/. f col-->


        </div><!--/. s-container-->


    </section><!--/. main-block-wrp-->


    <section class="s-wrp s-cover-img shrt-abt-wrp"><!--shrt-abt-wrp-->

        <div class="s-container"><!-- s-container-->

            <article class="s-wrp shrt-abt-content"><!--shrt-abt-content-->

                <h1>THE NEXT BEST THING IN INDUSTRIAL</h1>

                <p>We went above and beyond to create a fantastic experience. Perfectly crafted to suite your industrial business with almost unlimited options to get almost unlimited options.</p>




            </article><!--/. shrt-abt-content-->

        </div><!--/. s-container-->

    </section><!--/. shrt-abt-wrp-->


    <section class="s-wrp s-hi-pad clients-wrp"><!--clients-wrp-->

        <div class="s-container"><!-- s-container-->

            <h2 class="sub-title">our clients</h2>


            <article class="client-block s-wrp"><!--client-block-->

                <div class="owl-carousel clients-slide"><!--clients-slide-->

                    <div class="item"><!--item-->

                        <span class="client-logo"><img src="<?php echo base_url() ?>bootstrap/images/client1.png" alt="client"></span>

                    </div><!--/. item-->

                    <div class="item"><!--item-->

                        <span class="client-logo"><img src="<?php echo base_url() ?>bootstrap/images/client2.png" alt="client"></span>

                    </div><!--/. item-->


                    <div class="item"><!--item-->

                        <span class="client-logo"><img src="<?php echo base_url() ?>bootstrap/images/client3.png" alt="client"></span>

                    </div><!--/. item-->


                    <div class="item"><!--item-->

                        <span class="client-logo"><img src="<?php echo base_url() ?>bootstrap/images/client4.png" alt="client"></span>

                    </div><!--/. item-->

                    <div class="item"><!--item-->

                        <span class="client-logo"><img src="<?php echo base_url() ?>bootstrap/images/client5.png" alt="client"></span>

                    </div><!--/. item-->


                    <div class="item"><!--item-->

                        <span class="client-logo"><img src="<?php echo base_url() ?>bootstrap/images/client6.png" alt="client"></span>

                    </div><!--/. item-->

                    <div class="item"><!--item-->

                        <span class="client-logo"><img src="<?php echo base_url() ?>bootstrap/images/client7.png" alt="client"></span>

                    </div><!--/. item-->

                    <div class="item"><!--item-->

                        <span class="client-logo"><img src="<?php echo base_url() ?>bootstrap/images/client8.png" alt="client"></span>

                    </div><!--/. item-->


                    <div class="item"><!--item-->

                        <span class="client-logo"><img src="<?php echo base_url() ?>bootstrap/images/client9.png" alt="client"></span>

                    </div><!--/. item-->


                </div><!--/. clients-slide-->


            </article><!--/. client-block-->



        </div><!--/. s-container-->

    </section><!--/. clients-wrp-->



    <section class="s-wrp s-hi-pad parallax-apply testimonial-wrp"><!--testimonial-wrp-->

        <div class="s-container"><!-- s-container-->

            <h2 class="sub-title white-txt">Testimonials</h2>


            <article class="testimonial-block s-wrp"><!--testimonial-block-->

                <div class="owl-carousel testi-slide"><!--testi-slide-->

                    <div class="item"><!--item-->

                        <div class="s-wrp s-in"><!--f wrp-->

                            <span class="testi-au-img s-sec-block"><img src="<?php echo base_url() ?>bootstrap/images/team-2.jpg" alt="testimonial author image"></span>

                            <div class="testi-content testi-short s-wrp"><!--testi content-->

                                <div class="s-in"><!--f in-->

                                    <p class="testi-p">"I have been servicing pools for most of my life and have been in business for myself in the pool industry for 4 years.  I recently began using your products. I recommend your products to my colleagues and employees.Thanks again!  </p>

                                    <span class="s-sec-block testi-au">- Mitchell</span>

                                </div><!--/. f in-->

                            </div><!--/. testi content-->

                        </div><!--/. f wrp-->

                    </div><!--/. item-->


                    <div class="item"><!--item-->

                        <div class="s-wrp s-in"><!--f wrp-->

                            <span class="testi-au-img s-sec-block"><img src="<?php echo base_url() ?>bootstrap/images/team-3.jpg" alt="testimonial author image"></span>

                            <div class="testi-content testi-short s-wrp"><!--testi content-->

                                <div class="s-in"><!--f in-->

                                    <p class="testi-p">Just wanted to share how much I like your  products.  Crystal clear and no nasty chlorine/chemical odor. That's it.  Great results!</p>

                                    <span class="s-sec-block testi-au">- Donna</span>

                                </div><!--/. f in-->

                            </div><!--/. testi content-->

                        </div><!--/. f wrp-->

                    </div><!--/. item-->


                    <div class="item"><!--item-->

                        <div class="s-wrp s-in"><!--f wrp-->

                            <span class="testi-au-img s-sec-block"><img src="<?php echo base_url() ?>bootstrap/images/team-4.jpg" alt="testimonial author image"></span>

                            <div class="testi-content testi-short s-wrp"><!--testi content-->

                                <div class="s-in"><!--f in-->

                                    <p class="testi-p">This is what i am looking for.. I love it</p>

                                    <span class="s-sec-block testi-au">- Sandra Kent</span>

                                </div><!--/. f in-->

                            </div><!--/. testi content-->

                        </div><!--/. f wrp-->

                    </div><!--/. item-->


                </div><!--/. testi-slide-->


            </article><!--/. testimonial-block-->


        </div><!--/. s-container-->

    </section><!--/. testimonial-wrp-->
<div class="modal"></div>




