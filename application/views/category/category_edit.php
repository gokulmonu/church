<script>
    $(function () {
    $("#category_form").submit(function( event ) {
        event.preventDefault();
        var formData = getFormData();
        $.ajax({
            url: '<?php echo base_url()?>category/validate',
            type: 'POST',
            data:  formData,
            success: function (data) {
                if(data.success){
                   if(data.id){
                       window.location.href ='<?php echo base_url() ?>category/view/'+data.id;
                   }
                }else{
                    alert('failure');
                }

            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });

    });


    function getFormData() {

        var data = {};
        data['id'] = $('#id').val();
        data['name'] = $('#name').val();
        data['parent_id'] = $('#parent_id').val();
        data['description'] = $('#description').val();
        return data;
    }

    });
</script>

<body>

<div class="s-pgwrp"><!--s-pgwrp-->


<section class="s-wrp s-hi-pad"><!-- section wrp-->
    <div class="s-container"><!-- s-container-->

        <div class="s-wrp"><!--s-wrp-->

            <div class="s-row"><!--s row-->

                <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!--s col-->

                    <article class="s-wrp edit-block">

                        <h2 class="main-title">Add / Edit Categories</h2>

                    </article>
                </div>
                <!--/. s col-->

            </div>
            <!--/. s row-->

        </div>
        <!--/. s-wrp-->
    </div>
    <!--/. s-container-->
</section>
    <!--/. section wrp-->



<section class="s-wrp"><!-- section wrp-->
    <div class="s-container"><!-- s-container-->

        <div class="s-wrp"><!--s-wrp-->

            <div class="s-row"><!--s row-->

                <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!--s col-->

                    <form class="custom-form" id="category_form">
<input type="hidden" id="id" value="<?php echo isset($id)?$id:false ?>">
<input type="hidden" id="parent_id" value="<?php echo isset($parent_id)?$parent_id:false ?>">
                            <div class="s-form-group">

                                <label>Name:</label>
                                <input type="text" class="" id="name" placeholder="Enter Category Name" value="<?php echo isset($name)?$name:false ?>">

                            </div>

                            <div class="s-form-group">

                                <label>Description:</label>
                                <textarea id="description" placeholder="Describe about this category"><?php echo isset($description)?$description:false ?></textarea>

                            </div>

                            <div class="s-form-group">
                               <input type="submit" class="pg-btn" value="Save">
                            </div>

                        </form>







                </div>
                <!--/. s col-->

            </div>
            <!--/. s row-->

        </div>
        <!--/. s-wrp-->
    </div>
    <!--/. s-container-->
</section>
<!--/. section wrp-->
</div>
<!--/. s-pgwrp-->


</body>
</html>