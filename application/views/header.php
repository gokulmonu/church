<head>
    <title>&nbsp; &nbsp;GLATCO PVT LTD - Home</title>
    <meta property='og:site_name' content='&nbsp; &nbsp;GLATCO PVT LTD' />
    <meta property='og:title' content='&nbsp; &nbsp;GLATCO PVT LTD' />
    <meta property='og:description' content='Web Hosting by Bluehost' />
    <meta property='og:url' content='http://glatcomaldives.com/' />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="icon" type="image/png" sizes="32x32" href="fav.png">

    <!--stylesheets-->

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href=" <?php echo base_url() ?>bootstrap/css/font-awesome.css" />

    <link rel="stylesheet" type="text/css" href=" <?php echo base_url() ?>bootstrap/css/animate.css" />

    <link rel="stylesheet" type="text/css" href=" <?php echo base_url() ?>bootstrap/js/flex-slider/flexslider-min.css" />
    <link rel="stylesheet" type="text/css" href=" <?php echo base_url() ?>bootstrap/js/flex-slider/flexslider-content.css" />

    <link rel="stylesheet" type="text/css" href=" <?php echo base_url() ?>bootstrap/js/nav-responsive/nav-responsive.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/js/responsive-tabs/easy-responsive-tabs.css" />

    <link rel="stylesheet" type="text/css" href=" <?php echo base_url() ?>bootstrap/css/base.css" />
    <link rel="stylesheet" type="text/css" href=" <?php echo base_url() ?>bootstrap/css/s-controls.css" />

    <link rel="stylesheet" type="text/css" href=" <?php echo base_url() ?>bootstrap/css/custom.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/css/dropzone.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/css/iziModal.min.css"/>

    <link rel="stylesheet" type="text/css" href=" <?php echo base_url() ?>bootstrap/js/owl-carousel/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href=" <?php echo base_url() ?>bootstrap/js/owl-carousel/owl.theme.css" />
    <link rel="stylesheet" type="text/css" href=" <?php echo base_url() ?>bootstrap/js/owl-carousel/owl.transitions.css" />


    <!--/. stylesheets-->


    <!--js files-->
    <script type="text/javascript" src=" <?php echo base_url() ?>bootstrap/js/jquery-2_1_4.js"></script>


    <script type="text/javascript" src=" <?php echo base_url() ?>bootstrap/js/flex-slider/flex-slider.js"></script>
    <script type="text/javascript" src=" <?php echo base_url() ?>bootstrap/js/flex-slider/flex-slider-script.js"></script>

    <script type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/dropzone/dropzone.js"></script>


    <script  type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/responsive-tabs/easyResponsiveTabs.js"></script>
    <script  type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/responsive-tabs/tabs-script.js"></script>

    <!--<script type="text/javascript" src="<?php /*echo base_url() */?>bootstrap/js/fancy-box/jquery.fancybox.js"></script>
    <script type="text/javascript" src="<?php /*echo base_url() */?>bootstrap/js/fancy-box/fancy-box-script.js"></script>
    <script type="text/javascript" src="<?php /*echo base_url() */?>bootstrap/js/tool-tip/tooltipsy.min.js"></script>
    <script type="text/javascript" src="<?php /*echo base_url() */?>bootstrap/js/tool-tip/tooltipsy-script.js"></script>
    <script type="text/javascript" src="<?php /*echo base_url() */?>bootstrap/js/accordion/accordion.js"></script>-->

    <script type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/nav-responsive/nav-responsive.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/nav-responsive/nav-drop.js"></script>
   <!-- <script type="text/javascript" src="<?php /*echo base_url() */?>bootstrap/js/scroll-top.js"></script>-->



    <script src="<?php echo base_url() ?>bootstrap/js/auto-complete/jquery.autocomplete.js"></script>
    <script src="<?php echo base_url() ?>bootstrap/js/tree-view/bootstrap-treeview.js"></script>

    <script type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/izimodal/iziModal.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/owl-carousel/owl.carousel.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/owl-carousel/owl-script.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/owl-carousel/owl-thumbnail.js"></script>


    <!--/. js files-->




</head>


<body>

<div class="s-pgwrp"><!--s-pgwrp-->

    <section class="top-header-wrp s-wrp">  <!--top-header-wrp-->

        <div class="s-container"><!--s-container-->

            <div class="s-row"><!--s-row-->

                <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!--f col-->

                    <span class="q-addr">Glat Co. Pvt. Ltd Republic of maldives</span>

                </div><!--/. f col-->

                <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!--f col-->

                    <article class="top-social-media s-wrp"><!--top-social-media-->

                        <ul>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>

                        </ul>


                    </article><!--/. top-social-media-->

                </div><!--/. f col-->


            </div><!--/. s-row-->

        </div><!--/. s-container-->

    </section><!--/. top-header-wrp-->


    <header class="header-wrp s-wrp"><!--header-wrp-->

        <article class="logo-area s-wrp"><!--logo-area-->

            <div class="s-container"><!-- s-container-->

                <div class="s-row"><!--s-row-->

                    <div class="s-col-lg-4 s-col-md-4 s-col-sm-12 s-col-xs-12 site-bckg"><!--f col-->

                        <article class="logo-block"><!--logo-block-->

                            <a href="<?php echo base_url() ?>" class="logo-a"><img src="<?php echo base_url() ?>bootstrap/images/logo.png" alt="logo"></a>

                        </article><!--/. logo-block-->


                    </div><!--/. f col-->


                    <div class="s-col-lg-8 s-col-md-8 s-col-sm-12 s-col-xs-12"><!--f col-->

                        <article class="quick-contact-block"><!--quick-contact-block-->

                            <ul>


                                <li>

                                    <article class="each-quick-contact s-wrp"><!--each-quick-contact-->

                                        <div class="contact-block mail-block"><!--contact-block -->

                                            <span class="s-sec-block site-color">Send us a mail</span>
                                            <em class="s-sec-block">info@glatcomaldives.com</em>


                                        </div><!--/. contact-block -->


                                    </article><!--/. each-quick-contact-->



                                </li>


                                <li>

                                    <article class="each-quick-contact"><!--each-quick-contact-->

                                        <div class="contact-block fax-block"><!--contact-block -->

                                            <span class="s-sec-block site-color">fax us on</span>
                                            <em class="s-sec-block">3329378</em>


                                        </div><!--/. contact-block -->


                                    </article><!--/. each-quick-contact-->


                                </li>


                                <li>

                                    <article class="each-quick-contact s-wrp"><!--each-quick-contact-->

                                        <div class="q-call-wrp s-wrp"><!--q-call-wrp-->

                                            <em class="q-call-lbl">call us on</em>

                                            <span class="q-call-txt">3329378</span>

                                        </div><!--/. q-call-wrp-->

                                    </article><!--/. each-quick-contact-->


                                </li>



                            </ul>



                        </article><!--/. quick-contact-block-->


                    </div><!--/. f col-->


                </div><!--/. s-row-->

            </div><!--/. s-container-->

        </article>	<!--/. logo-area-->


        <div class="navigation-wrp s-wrp"><!--navigation-wrp-->

            <div class="s-container"><!--s-container-->

                <nav class="s-nav s-wrp"><!--nav-->

                    <ul class="s-nav-ul" data-type="navbar">
                        <li><a href="<?php echo base_url() ?>">Home</a></li>
                        <li class="s-nav-dropdwn">
                            <a href="<?php echo base_url()."about-us"; ?>">About us</a>

                        </li>

                        <li class="s-nav-dropdwn">

                            <a href="<?php echo base_url()."services"; ?>">service</a>

                                <!-- <ul class="s-dropdwn-menu">
                                    <li><a href="service-single.html">Oil Well Drilling </a></li>
                                    <li><a href="service-single.html">Water Well Drilling </a></li>
                                    <li><a href="service-single.html">Equipment &amp; Material supply </a></li>
                                    <li><a href="service-single.html">chemicals </a></li>
                                    <li><a href="service-single.html">PETRO CHEMICAL SUPPLY </a></li>
                                    <li><a href="service-single.html">FERTILIZER SUPPLY</a></li>
                                </ul>-->

                        </li>

                        <!--<li><a href="#">features</a></li>

                        <li><a href="projects.html">projects</a></li>-->

                        <li><a href="<?php echo base_url()?>contact-us">Contact Us</a></li>
                        <li class="cart-icon">
                            <a href="<?php echo base_url()?>cart/view"">
                                <i class="fa fa-shopping-cart"></i>
                            </a>
                            <span class="cart-counter"><?php /*echo $this->core_lib->getCartCount() */?></span>
                        </li>

                        <?php

                        $isLoggedIn = $this->core_lib->getLoginUserId();
                        $admin = false;
                        $userGroup= $this->core_lib->getUserGroup();
                        if(in_array("admin",$userGroup)){
                            $admin  = true;
                        }
                        if($admin){?>

                        <li class="s-nav-dropdwn">
                            <a href="#">Admin</a>

                            <ul class="s-dropdwn-menu animated hide">
                                <li><a  href="<?php echo base_url()?>category" class="profile ul-category">Categories</a></li>
                                <li><a href="<?php echo base_url()?>product/add/" class="profile ul-product">Add Product</a></li>
                                <li><a href="<?php echo base_url()?>aboutus/add/" class="profile ul-product">AboutUs</a></li>
                                <li><a href="<?php echo base_url()?>logout" class="signout ul-signout">Signout</a></li>


                            </ul>
                        </li>
                            <?php

                            }elseif($isLoggedIn){?>
                        <li class="s-nav-dropdwn">
                            <a href="#">Hi ,Welcome</a>

                            <ul class="s-dropdwn-menu animated hide">
                                <li><a href="<?php echo base_url()?>logout" class="signout ul-signout">Signout</a></li>
                            </ul>
                        </li>
                                <?php
                                } else{
                                    ?>
                                    <li>
                                        <a href="<?php echo base_url() ?>login" class="account">Login</a>
                                    </li>

                                    <?php
                                }
                                ?>

                    </ul>
<!--
                    <a href="contact.html" class="get-quote pg-btn"><em>get a quote</em></a>

-->
                </nav><!--/. nav-->

            </div> <!--/. s-container-->

        </div><!--/. navigation-wrp-->

    </header><!--/. header-wrp-->
   