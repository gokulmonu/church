<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Core_lib
{


    protected $userId;
    /**
     * __construct
     *
     * @return void
     **/
    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->library('default_session');

    }

    public function getLoginUserId()
    {
        $userId = $this->CI->default_session->get('user_id');
        return $userId;
    }

    public function getUserGroup($userId = 0)
    {
        if(empty($userId)){
            $userId = $this->getLoginUserId();
        }
        $this->CI->load->model('user/user_model');
        $groupArr = $this->CI->user_model->getUserGroup($userId);
        return $groupArr;

    }

    public function getUserName($userId = 0)
    {
        if(empty($userId)){
            $userId = $this->getLoginUserId();
        }
        $this->CI->load->model('user/user_model');
        $userData = $this->CI->user_model->getData($userId);
        return isset($userData['first_name'])?$userData['first_name'] : false;

    }

    public function getCartCount()
    {
        $this->CI->load->model('product_cart_model');
        $userId = $this->getLoginUserId();
        $cartArr = $this->CI->product_cart_model->getCart($userId);
        return count($cartArr);
    }

}
